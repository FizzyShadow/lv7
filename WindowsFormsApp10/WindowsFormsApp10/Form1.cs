﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp10
{
    public partial class Form1 : Form
    {
        Graphics g;
        List<Pen> pens = new List<Pen>();
        public Form1()
        {
            
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
            pens.Add(new Pen(Color.Red, HSB1.Value));
            pens.Add(new Pen(Color.Green, HSB1.Value));
            pens.Add(new Pen(Color.Blue, HSB1.Value));
        }
    

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            
            
            foreach(RadioButton radio in groupBox1.Controls)
            {
                if (radio.Checked) {
                    if ( radio.Text == "Triangle")
                    {
                        Triangle t = new Triangle(HSB1.Value);
                        foreach (RadioButton radioo in groupBox2.Controls)
                        {
                            if (radioo.Checked)
                            {
                                if (radioo.Text == "Red")
                                {
                                    t.draw(g, pens[0], e.X, e.Y);
                                }
                                else if (radioo.Text == "Green")
                                {
                                    t.draw(g, pens[1], e.X, e.Y);
                                }
                                else if (radioo.Text == "Blue")
                                {
                                    t.draw(g, pens[2], e.X, e.Y);
                                }
                            }
                        }
                    }
                    else if (radio.Text == "Circle")
                    {
                        Circle t = new Circle(HSB1.Value);
                        t.draw(g, pens[0], e.X, e.Y); foreach (RadioButton radioo in groupBox2.Controls)
                        {
                            if (radioo.Checked)
                            {
                                if (radioo.Text == "Red")
                                {
                                    t.draw(g, pens[0], e.X, e.Y);
                                }
                                else if (radioo.Text == "Green")
                                {
                                    t.draw(g, pens[1], e.X, e.Y);
                                }
                                else if (radioo.Text == "Blue")
                                {
                                    t.draw(g, pens[2], e.X, e.Y);
                                }
                            }
                        }
                    }
                    else if (radio.Text == "Square")
                    {
                        Square t = new Square(HSB1.Value);
                        foreach (RadioButton radioo in groupBox2.Controls)
                        {
                            if (radioo.Checked)
                            {
                                if (radioo.Text == "Red")
                                {
                                    t.draw(g, pens[0], e.X, e.Y);
                                }
                                else if (radioo.Text == "Green")
                                {
                                    t.draw(g, pens[1], e.X, e.Y);
                                }
                                else if (radioo.Text == "Blue")
                                {
                                    t.draw(g, pens[2], e.X, e.Y);
                                }
                            }
                        }
                    }
                }
                
            }

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {
            
        }
    }

    class Circle
    {
        int r;
        public Circle(int hs = 10)
        {
            r = hs;
        }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawEllipse(p, x, y, r, r);
        }
    }

    class Square
    {
        int a;
        public Square(int hs = 10)
        {
            a = hs;
        }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawRectangle(p, x, y, a, a);
        }
    }

    class Triangle
    {
        int a;
        public Triangle(int hs = 10)
        {
            a = hs;
        }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawLine(p, x , y, x + a, y);
            g.DrawLine(p, x, y, x+a, y - a);
            g.DrawLine(p, x+a, y - a, x+a, y);
        }
    }
}
