﻿using System;
using System.Collections.Generic;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IPlayable> playables = new List<IPlayable>();
            Random random = new Random();
            bankovni_racun brc = new bankovni_racun("Mate", 5000, 2000);
            mobilni_racun mrc = new mobilni_racun("0994356223", 2500, 20);
            playables.Add(brc);
            playables.Add(mrc);
            foreach (IPlayable obj in playables)
            {
                obj.dodaj(random.Next(10, 100));

            }
            foreach (IPlayable obj in playables)
            {
                obj.Oduzmi(random.Next(1, 10));

            }
            foreach (IPlayable obj in playables)
            {
                Console.WriteLine(obj.pregled());

            }
        }

    }

    class bankovni_racun : IPlayable
    {
        string ime;
        double Iznos;
        double Max_minus;
        public bankovni_racun(string gIme, double gIznos, double gMax_minus)
        {
            ime = gIme;
            Iznos = gIznos;
            Max_minus = gMax_minus;
        }
        public double pregled()
        {
            return Iznos;
        }
        public void dodaj(double gDodaj)
        {
            Iznos += gDodaj;
        }
        public void Oduzmi(double gOduzmi)
        {
            Iznos -= gOduzmi;
        }

    }

    class mobilni_racun : IPlayable
    {
        string Broj;
        double Iznos;
        double Cijena_poruke;
        public mobilni_racun(string gBroj, double gIznos, double gCijena_poruke)
        {
            Broj = gBroj;
            Iznos = gIznos;
            Cijena_poruke = gCijena_poruke;
        }
        public double pregled()
        {
            return Iznos;
        }
        public void dodaj(double gDodaj)
        {
            Iznos += gDodaj;
        }
        public void Oduzmi(double gOduzmi)
        {
            Iznos -= gOduzmi;
        }
    }

    interface IPlayable
    {
        double pregled();
        void dodaj(double gDodaj);
        void Oduzmi(double gOduzmi);
    }
}
